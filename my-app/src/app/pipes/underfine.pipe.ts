import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'underfine'
})
export class UnderfinePipe implements PipeTransform {

  transform(value: undefined | any, message: string): any {
    if (value != undefined)
      return value;
    return message;
  }

}
