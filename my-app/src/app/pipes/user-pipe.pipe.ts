import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../models/user';

@Pipe({
  name: 'userPipe'
})
export class UserPipePipe implements PipeTransform {

  transform(value: User[]): string {
    if(value.length==0)
      return 'No users were found';
    let stringBuilder:string[] = [];
    for(let user of value){
      stringBuilder.push(`${user.lastName} ${user.firstName}`);
    }
    return stringBuilder.join("; ");
  }

}
