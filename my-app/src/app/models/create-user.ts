export interface CreateUser {
    teamId?: number,
    firstName: string,
    lastName: string,
    email: string,
    birthDay: Date
}