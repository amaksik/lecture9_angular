import {TaskState} from './task-enum';
export interface CreateTask{
    name: string;
    description: string;
    state: TaskState,
    performerId: number,
    projectId: number
}