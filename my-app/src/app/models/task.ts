import {TaskState} from './task-enum';
export interface Task{
    id: number;
    name: string;
    description: string;
    state: TaskState,
    createdAt: Date,
    finishedAt?: Date | null,
    performerId: number,
    projectId: number
}