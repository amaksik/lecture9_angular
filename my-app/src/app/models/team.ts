import { User } from "./user";

export interface Team {
    id: number,
    name: string,
    members: User[],
    createdAt: Date
}