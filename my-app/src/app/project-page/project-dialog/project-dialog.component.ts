import { Component, OnInit, Inject } from '@angular/core';
import { CreateProject } from 'src/app/models/create-project';
import { Project } from 'src/app/models/project';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
  selector: 'app-project-dialog',
  templateUrl: './project-dialog.component.html',
  styleUrls: ['./project-dialog.component.scss']
})
export class ProjectDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ProjectDialogComponent>, @Inject(MAT_DIALOG_DATA) public project: Project) {
  }
  data: CreateProject | Project = { authorId: 0, teamId:0, name: '', description: '', tasks: [], deadline:undefined }
  ngOnInit(): void {
    if (this.project != null) {
      this.data = this.project;
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
