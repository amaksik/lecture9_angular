import { CreateTask } from './../../models/create-task';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { FormComponent } from 'src/app/guard/form.guard';
import { NgForm } from '@angular/forms';
import { TaskService } from 'src/app/services/task-service.service';
import { TaskState } from 'src/app/models/task-enum';

@Component({
  selector: 'app-create-task-dialog',
  templateUrl: './create-task-dialog.component.html',
  styleUrls: ['./create-task-dialog.component.scss']
})
export class CreateTaskDialogComponent implements OnInit, FormComponent {

  constructor(public taskService: TaskService, private router: Router,
    private route: ActivatedRoute) {
  }
  submitedOrGood: boolean = false;
  canExit(): boolean | Observable<boolean> | Promise<boolean> {
    if (this.submitedOrGood)
      return true;
    if (confirm("Do you want to leave?")) {
      return true
    } else {
      return false
    }
  }
  task: CreateTask = { name: '', description: '', state: TaskState.Backlog, performerId: 0, projectId: 0 };
  ngOnInit(): void {
  }
  onSubmit(data: CreateTask) {
    debugger;
    this.submitedOrGood = true;
    this.taskService.createTask$(data).subscribe();
    this.router.navigate(['../../created'], { relativeTo: this.route }).then(() => window.location.reload());
  }
  onNoClick(updateForm: NgForm): void {
    this.submitedOrGood = (!updateForm.dirty && updateForm.untouched)!;
    this.router.navigate(['../../none'], { relativeTo: this.route });
  }
}
