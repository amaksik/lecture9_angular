import { CreateTeam } from './../models/create-team';
import { Team } from './../models/team';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TeamService {
  private route = 'https://localhost:5001/api/team';
  constructor(private http: HttpClient) { }

  getTeams$(): Observable<Team[]> {
    return this.http.get<Team[]>(this.route);
  }
  createTeam$(team: CreateTeam): Observable<Team> {
    const headers = { 'content-type': 'application/json' };
    return this.http.post<Team>(this.route, JSON.stringify(team), { headers: headers });
  }
  getById$(id:number): Observable<Team>{
    return this.http.get<Team>(this.route+`/${id}`);
  }
  updateTeam$(team: Team): Observable<any> {
    const headers = { 'content-type': 'application/json' };
    return this.http.put(this.route, JSON.stringify(team), { headers: headers });
  }
}
